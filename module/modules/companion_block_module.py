from PIL import ImageDraw
from dataclasses import dataclass, field
from typing import *

from dataclasses_json.core import Json
from pyhocon import ConfigTree

from dataclasses_config import Config, Path, Settings
from hammerdraw.compilers import CompilerError
from hammerdraw.interfaces import GradientPart
from hammerdraw.util import ImageType, get_color, ColorType

from ..mixins.image_row_mixin import ImageRowMixin, ImageObjectAttributes
from ..mixins.multipart_module_mixin import MultiPartModuleMixin

@dataclass(frozen=Settings.frozen)
class GradientConfig(GradientPart, Config):
    position: float
    color: ColorType

@dataclass(frozen=Settings.frozen)
class CompanionHealth(Config):
    constant: List[ImageObjectAttributes]
    temporary: List[ImageObjectAttributes]

@dataclass(frozen=Settings.frozen)
class CompanionBlockParameters(Config):
    skills_min: int
    skills_max: int
    skills_titles: List[str]
    health: CompanionHealth
    
    header_height: int
    header_bg_grad: List[GradientPart]
    header_bg_height: int
    underscore_color: ColorType
    underscore_width: int
    
    skills_horizontal_separator: int
    skills_row_height: int
    skill_point_image_width: int
    skill_point_purchased_image: str
    skill_point_free_image: str
    skill_box_image: str
    skill_box_max_height: int
    
    health_row_height: int
    health_point_image_width: int
    
    @classmethod
    def from_dict(cls, kvs: Json, **kwargs) -> 'CompanionBlockParameters':
        kvs.setdefault('header_bg_height', kvs.get('header_height'))
        kvs.setdefault('header_bg_grad', list())
        lst = kvs['header_bg_grad']
        for i in range(len(lst)):
            lst[i] = GradientConfig.from_config(ConfigTree(lst[i]))
        
        # noinspection PyTypeChecker
        return super().from_dict(kvs, **kwargs)

class CompanionBlockModule(MultiPartModuleMixin[CompanionBlockParameters], ImageRowMixin):
    base_name = 'companions'
    config_class = CompanionBlockParameters
    
    def _compile(self, base: ImageType):
        params = self._get_parameters()
        if (not params.skills_titles):
            raise CompilerError("Missing required skills properties")
        
        if (len(params.header_bg_grad) >= 2):
            dh = params.header_height - params.header_bg_height
            gradient_base = self.parent.get_gradient_base(self.width, params.header_bg_grad)
            gradient = gradient_base.resize((self.width, params.header_bg_height))
            _gradient = gradient.convert('RGB')
            base.paste(_gradient, (0, dh // 2), gradient)
        
        y = params.header_height
        drawer = ImageDraw.ImageDraw(base)
        line_style = dict(fill=get_color(params.underscore_color), width=params.underscore_width)
        drawer.line([(0, y), (self.width, y)], **line_style)
                
        x = 0
        w = (self.width - params.skills_horizontal_separator * (len(params.skills_titles) - 1)) // len(params.skills_titles)
        y0 = y
        
        td = self.get_text_drawer(base, 'fonts/skills', 'fonts/skillsStyle')
        image_objects = \
        [
            ImageObjectAttributes(params.skills_min, params.skill_point_purchased_image), 
            ImageObjectAttributes(params.skills_max - params.skills_min, params.skill_point_free_image),
        ]
        
        box_path = Path(self.parent.sources_directory, params.skill_box_image)
        
        for skill in params.skills_titles:
            y = y0
            
            td.print_in_region((x, y, w, params.skills_row_height), skill)
            y += params.skills_row_height
            
            self.insert_image_row(base, image_objects, image_width=params.skill_point_image_width, region=(x, y, w, params.skills_row_height))
            y += params.skills_row_height
            
            self.parent.insert_image_scaled(base, (x, y, w, params.skill_box_max_height), box_path, scale_func=min, center=True, foreground=True)
            y += params.skill_box_max_height
            
            x += w + params.skills_horizontal_separator
        
        self.insert_image_row(base, params.health.constant, region=(0, y, self.width, params.health_row_height), image_width=params.health_point_image_width)
        y += params.health_row_height
        self.insert_image_row(base, params.health.temporary, region=(0, y, self.width, params.health_row_height), image_width=params.health_point_image_width)
        y += params.health_row_height
        
        return y

__all__ = \
[
    'CompanionBlockParameters',
    'CompanionBlockModule',
]
