from functools import partial

from dataclasses import dataclass
from typing import *

from dataclasses_config import Config, Settings
from hammerdraw.modules.charsheets import ExtrasBlockModule
from hammerdraw.text_drawer.text_funcs import capitalize_first
from hammerdraw.util import join_list
from hammerdraw.modules.core import ContainerModule, ImageModule, TextModule

from ..mixins import ParameterizedModuleMixin

@dataclass(frozen=Settings.frozen)
class SuiteParameters(Config):
    pass

class SuiteModule(ParameterizedModuleMixin[SuiteParameters], ContainerModule):
    module_name = 'suite'
    config_class = SuiteParameters
    
    def initialize(self, *args, **kwargs):
        super().initialize(self.modules, *args, **kwargs)
    
    modules = \
    [
        (ImageModule, dict(name='image', path_from_config=True, format_path=True, foreground=True, scale_func=min, center=True)),
        
        # Headers
        (TextModule, dict(name='title', scale_field='titleFontSizeScale')),
        (TextModule, dict(name='tags', convert_to_str=partial(join_list, separator=' ♦ ', formatter=capitalize_first))),
        
        # Extras: Left
        (ExtrasBlockModule, dict(name='left')),
        (ExtrasBlockModule, dict(name='right')),
    ]

__all__ = \
[
    'SuiteModule',
    'SuiteParameters',
]
