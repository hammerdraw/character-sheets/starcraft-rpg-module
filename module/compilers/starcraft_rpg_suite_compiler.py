from hammerdraw.compilers import CompilerBase
from hammerdraw.modules.charsheets.modules.starcraft_rpg import *

class StarCraftRpgSuiteCompiler(CompilerBase):
    module_name = 'charsheets'
    compiler_type = 'starcraft-rpg-suite'
    default_extension_priorities = [ 'json', 'json5' ]
    
    modules = \
    [
        SuiteModule,
    ]
    
    def _get_base_filename(self):
        return super()._get_base_filename() \
            .replace('{level}', str(self.get_from_raw('level'))) \
            .replace('{background}', 'transparent' if self.get_from_config('transparent', default=True) else 'background') \
    
    def _get_output_name(self) -> str:
        title: str = self.get_from_raw('suite/title')
        locale:  str = self.get_from_raw('locale')
        return f"{title} {locale.upper()}"
    
    def _generate_filename(self, *args, **kwargs) -> str:
        return super()._generate_filename(self._get_output_name())

__all__ = \
[
    'StarCraftRpgSuiteCompiler',
]

def main():
    from hammerdraw.util import setup
    from hammerdraw import HammerDrawConfig
    
    config = HammerDrawConfig.default().with_root()
    print(config.text_drawer)
    setup(main_config=config)
    
    for model in StarCraftRpgSuiteCompiler(config=config).isearch():
        compiler = StarCraftRpgSuiteCompiler(config=config)
        compiler.open(model, ignore_schema=True)
        compiler.compile()
        compiler.save_compiled()
    
    return 0

if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)
