from hammerdraw.compilers import CompilerBase
from hammerdraw.modules.core import ImageModule, TextModule
from hammerdraw.modules.charsheets import *
from hammerdraw.modules.charsheets.modules.starcraft_rpg import *

class StarCraftRpgCharSheetCompiler(CompilerBase):
    module_name = 'charsheets'
    compiler_type = 'starcraft-rpg'
    default_extension_priorities = [ 'json', 'json5' ]
    
    modules = \
    [
        # Headers
        (TextModule, dict(name='headers/skills')),
        (TextModule, dict(name='headers/companions')),
        (TextModule, dict(name='headers/version', field='version')),
        
        # Caption
        (TextModule, dict(name='headers/player')),
        (TextModule, dict(name='headers/person')),
        (TextModule, dict(name='headers/personality')),
        (TextModule, dict(name='headers/motivation')),
        (CaptionModule, dict(name='left')),
        (CaptionModule, dict(name='right')),
        
        # Skills
        (SkillBlockModule, dict(name='physical')),
        (SkillBlockModule, dict(name='mental')),
        (SkillBlockModule, dict(name='social')),
        
        # Extras: Left
        (ExtrasBlockModule, dict(name='equipment',   after=None)),
        (ExtrasBlockModule, dict(name='positives',   after='equipment')),
        (ExtrasBlockModule, dict(name='negatives',   after='positives')),
        
        # Extras: Right
        # (ImageModule,       dict(name='image',       after=None, field='faction', path_from_config=True, format_path=True, foreground=True, scale_func=min, center=True)),
        SuiteModule,
        (ExtrasBlockModule, dict(name='exp',         after=None)),
        (ExtrasBlockModule, dict(name='resources',   after='exp')),
        (ExtrasBlockModule, dict(name='reputation',  after='resources')),
        
        # Stats
        (StatsBlockModule, dict(name='armor',  after=None)),
        (StatsBlockModule, dict(name='health', after='armor')),
        (StatsBlockModule, dict(name='will',   after='health')),
        (StatsBlockModule, dict(name='ammo',   after='will')),
        
        # Companions
        (CompanionBlockModule, dict(name='left')),
        (CompanionBlockModule, dict(name='middle')),
        (CompanionBlockModule, dict(name='right')),
    ]
    
    def _get_output_name(self) -> str:
        faction: str = self.get_from_raw('faction')
        locale:  str = self.get_from_raw('locale')
        version: str = self.get_from_raw('version')
        return f"{faction.title()} {locale.upper()} {version}"
    
    def _generate_filename(self, *args, **kwargs) -> str:
        return super()._generate_filename(self._get_output_name())

__all__ = \
[
    'StarCraftRpgCharSheetCompiler',
]

def main():
    from hammerdraw.util import setup
    from hammerdraw import HammerDrawConfig
    
    config = HammerDrawConfig.default().with_root()
    setup(main_config=config)
    compiler = StarCraftRpgCharSheetCompiler(config=config)
    compiler.open('dominion-ru', ignore_schema=True)
    compiler.compile()
    compiler.save_compiled()
    
    return 0

if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)
